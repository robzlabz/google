# A Dead Simple Google web Scraping

## Installion
To install library to your project, you just need to install robzlabz/google to your composer dependency
```
composer require robzlabz/google
```

## Image Search
```php
use Golek\Google\Image;

$results = (new Image)->get('keyword');

// if you want to show page 2 of image, you can use ...->get('keyword',2);
// return 100 list of images
```

## Search Index 
```php
use Golek\Google\Index;

dd((new Index)->get('google.com'));

// return array index search and index image
```

