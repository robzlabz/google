<?php 

namespace Golek\Bing;

use Golek\BaseImage;
use Golek\http\Connection;

class Image extends BaseImage {

	protected $urlParams;

	public function __construct() {
		$this->urlParams = [
			'FORM' => 'HDRSC2'
		];
	}

	public function get($keyword) {

		$this->urlParams['q'] = urlencode($keyword);

		$url = 'https://www.bing.com/images/search?'.http_build_query($this->urlParams);

        $http = new Connection;
        $html = $http->get($url);

        return $this->parse($html)->pretty();
	}

	private function parse($html) {

		file_put_contents(public_path('sample.txt'), $html);

		dump($this->between($html, 'purl&quot;:&quot;', '&quot;'));
		dump($this->between($html, 'murl&quot;:&quot;', '&quot;'));
		dump($this->between($html, 'turl&quot;:&quot;', '&quot;'));
		dump($this->between($html, '<li tabindex="0">', '</li>'));

		return $this;
	}

	private function pretty() {
		return 0;
	}
}