<?php

namespace Golek\Bing;
use Golek\http\Connection;

class Tag {
	public function get($string)
	{

		$params = http_build_query([
			'pt'	=> 'page.home',
			'mkt'	=> 'en-us',
			'qry'	=> urlencode($string),
			'cp'	=> 4,
			'cvid'	=> '620b40501dd944a9b18a375836f2b302'
		]);

		$url = 'http://www.bing.com/AS/Suggestions?'.$params;

		$http = new Connection;
		$http = $http->get($url);

		$results = [];
		if( ! empty($http)) {
			$xResponse = explode('<div class="sa_tm">',$http);
			foreach($xResponse as $i => $data) {
				if($i == 0) continue;
				list($tag, $dump) = explode('</div>', $data, 2);
				$results[] = strip_tags($tag);
			}
		}
		return $results;
	}
}
