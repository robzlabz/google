<?php

namespace Golek\http;

class Connection {

    private $useragent;
    private $proxies;

    private function proxyFromFile() {
        $this->proxies = array_filter(explode("\n", file_get_contents(__DIR__."/proxies.txt")));
        return $this->proxies;
    }

    private function useragentFromFile() {
        $this->useragent = array_filter(explode("\n", file_get_contents(__DIR__."/user_agent.txt")));
        return $this->useragent;
    }

    public function setProxy(array $proxies){
        $this->proxies = $proxies;
    }

    public function setUseragent(array $useragent) {
        $this->useragent = $useragent;
    }

    public function get($url, $useProxy = false, $retry = 0) {

        $proxies    = $this->proxies;
        $useragents = $this->useragent;

        if(empty($proxies)) 
            $proxies = $this->proxyFromFile();

        if(empty($useragents)) 
            $useragents = $this->useragentFromFile();

        shuffle($proxies);
        shuffle($useragents);

        $proxy = $proxies[0];
        $useragent = $useragents[0];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);

        if($useProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
            if(stripos($proxy, '1080') !== false) {
                curl_setopt($ch, CURLOPT_PROXYTYPE,CURLPROXY_SOCKS5);
            }
        }

        $result = curl_exec($ch);
        if(curl_errno($ch)) {
            if($retry > 5) {
                return false;
            }
            curl_close($ch);
            return $this->get($url, true, $retry+1);
        }
        curl_close($ch);

        return $result;
    }
}
