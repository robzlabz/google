<?php

namespace Golek\Google;

use Golek\http\Connection;

class Index {

	private $proxies;
    private $useragents;

    public function fix($domain)
    {
        return str_replace(['http://','https://','/'],'', $domain);
    }

	public function get($domain) {
		return [
			'search' => $this->search($domain),
			'image'	 => $this->image($domain)
		];
	}

	public function search($domain) {
		$url = 'https://www.google.com/search?q=site:'.$this->fix($domain);

		$http = new Connection;
		if(!empty($this->proxies)) $http->setProxy($this->proxies);
        if(!empty($this->useragents)) $http->setUseragent($this->useragents);
		$html = $http->get($url);

		return $this->parseSearch($html);
	}

	public function setProxy(array $proxies) {
        $this->proxies = $proxies;
    }

    public function setUseragent(array $useragents) {
        $this->useragents = $useragents;
    }

	private function parseSearch($html) {
		try {
            if(stripos($html, 'did not match any') !== false) {
                return 0;
            } else if(strpos($html, '<div id="resultStats">') !== false) {
                list($dump, $html) = explode('<div id="resultStats">', $html);
                list($index, $dump) = explode('<nobr>', $html);
                return preg_replace('/[^0-9]/', '', $index);
            } else {
                return '?';
            }
        } catch (Exception $e) {
            return '?';
        }
	}

	public function image($domain) {
		$url = 'https://www.google.com/search?q=site:'.$this->fix($domain).'&tbm=isch&sout=1';

		$http = new Connection;
		$html = $http->get($url);

		return $this->parseImage($html);
	}

	private function parseImage($html) {
		try {
            if(stripos($html, 'did not match any') !== false ){
                return 0;
            } else if(strpos($html, 'align="right"><font size="-1">') !== false) {
                list($dump, $html) = explode('align="right"><font size="-1">', $html);
                list($index, $dump) = explode('(<b>', $html);
                return preg_replace('/[^0-9]/', '', $index);
            } else {
                return '?';
            }
        } catch (Exception $e) {
            return '?';
        }
	}

}
