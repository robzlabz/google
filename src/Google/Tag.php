<?php

namespace Golek\Google;

use Golek\http\Connection;

class Tag {

	private $proxies;
    private $useragents;

	public function get($string)
	{
		$params = http_build_query(array(
			'q'			=> urlencode($string),
			'client'	=> 'chrome',
			'hl'		=> 'en-us'
		));
		$url = 'http://suggestqueries.google.com/complete/search?'.$params;

        $http = new Connection();
        if(!empty($this->proxies)) $http->setProxy($this->proxies);
        if(!empty($this->useragents)) $http->setUseragent($this->useragents);
        $http = $http->get($url);

        if($json = json_decode($http,true)) {
        	return isset($json[1]) ? $json[1] : [];
        }
        return [];
	}

	public function setProxy(array $proxies) {
        $this->proxies = $proxies;
    }

    public function setUseragent(array $useragents) {
        $this->useragents = $useragents;
    }
}
