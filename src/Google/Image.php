<?php

namespace Golek\Google;

use Golek\http\Connection;
use Golek\Base\BaseImage;

class Image extends BaseImage {

    private $jsonResult;
    private $prettyResult;
    private $urlParams;
    private $tld = 'com';
    private $imageMode = 0; // 0:none, 1:potrait, 2:lanscape

    private $proxies;
    private $useragents;

    public function __construct() {
        $this->urlParams = [
            'site'  => 'imghp',
            'tbm'   => 'isch',
            'start' => 0,
        ];
    }

    public function setImageSize($size) {
        $gsize = array(
            ''                      => 'Default',
            'isz:l'                 => 'Large',
            'isz:m'                 => 'Medium',
            'isz:i'                 => 'Icon',
            'isz:lt,islt:qsvga'     => '400x300',
            'isz:lt,islt:vga'       => '640x680',
            'isz:lt,islt:svga'      => '800x600',
            'isz:lt,islt:xga'       => '1024x768',
            'isz:lt,islt:2mp'       => '2 MP',
            'isz:lt,islt:4mp'       => '4 MP',
            'isz:lt,islt:6mp'       => '5 MP',
            'isz:lt,islt:8mp'       => '8 MP',
            'isz:lt,islt:10mp'      => '10 MP',
            'isz:lt,islt:12mp'      => '12 MP',
            'isz:lt,islt:15mp'      => '15 MP',
            'isz:lt,islt:20mp'      => '20 MP',
            'isz:lt,islt:40mp'      => '40 MP',
            'isz:lt,islt:70mp'      => '70 MP'
        );
    }

    public function onlyPotrait() {
        $this->imageMode = 1;
        return $this;
    }

    public function onlyLanscape() {
        $this->imageMode = 2;
        return $this;
    }

    // Type : face, photo, clipart, lineart, animated
    public function setImageType($filter) {
        $this->urlParams['tbs'] = 'itp:'.$filter;
        return $this;
    }

    public function setCountry($country) {

        $countryTld = [
            'global'    => 'com',
            'indonesia' => 'co.id',
            'japan'     => 'co.jp',
            'german'    => 'de',
            'france'    => 'fr',
            'australia' => 'co.au',
        ];

        $this->tld = 'com';
        if(!array_key_exists($countryTld, $country)) {
            $this->tld = $countryTld[$country];
        }
    }

	public function get($keyword, $page = 1) {

        $this->urlParams['q'] = urlencode($keyword);
        $this->urlParams['ijn'] = $page-1;

        $url = 'https://www.google.'.$this->tld.'/search?'.http_build_query($this->urlParams);

        $http = new Connection;
        if(!empty($this->proxies)) $http->setProxy($this->proxies);
        if(!empty($this->useragents)) $http->setUseragent($this->useragents);
        $html = $http->get($url);

        return $this->parse($html)->pretty();
	}

    public function setProxy(array $proxies) {
        $this->proxies = $proxies;
    }

    public function setUseragent(array $useragents) {
        $this->useragents = $useragents;
    }

	public function parse($html) {

        if(stripos($html, 'class="rg_meta notranslate">') !== false) {
            $divider = 'class="rg_meta notranslate">';
        } else {
            $divider = 'class="rg_meta">';
        }

        $f = explode($divider, $html);
        $json = [];
        foreach($f as $i => $j) {
            if($i > 0) {
                list($js, $dmp) = explode('}</div', $j);
                $json[] = $js . '}';
            }
        }
        $this->jsonResult = json_decode('[' . implode(',', $json) . ']');

        return $this;
    }

    public function pretty() {
        $lists = [];
        if( ! empty($this->jsonResult)) {
            foreach($this->jsonResult as $api) {

                if($this->imageMode == 1) {
                    // potrait
                    if($this->have($api->ow) > $this->have($api->oh)) {
                        continue;
                    }
                } elseif($this->imageMode == 2) {
                    // lanspace
                    if($this->have($api->ow) < $this->have($api->oh)) {
                        continue;
                    }
                }

                $lists[] = [
                    'title'         => $this->have($api->pt),
                    'description'   => $this->have($api->s),
                    'url'           => $this->have($api->ru),
                    'source'        => $this->have($api->isu),
                    'thumb'         => $this->have($api->tu),
                    'thumb_width'   => $this->have($api->tw),
                    'thumb_height'  => $this->have($api->th),
                    'image'         => $this->have($api->ou),
                    'width'         => $this->have($api->ow),
                    'height'        => $this->have($api->oh),
                ];
            }
        }

        $lists = $this->toObject($lists);

        $this->prettyResult = $lists;
        return $lists;
    }


}
