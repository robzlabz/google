<?php 

namespace Golek\Base;

class BaseImage {

	protected function have(&$var) {
        if(isset($var) && !empty($var)) {
            return $var;
        }
    }

    protected function toObject($array) {
    	return json_decode(json_encode($array));
    }

    protected function between($string, $start, $end) {
    	$a = explode($start, $string);
    	$lists = [];
    	foreach($a as $k => $v) {
    		if($k == 0) continue;
    		list($b, $c) = explode($end, $v);
    		$lists[] = $b;
    	}
    	return $lists;
    }

}