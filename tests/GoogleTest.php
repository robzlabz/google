<?php 

class GoogleTest extends PHPUnit_Framework_TestCase {

    public function testAssertTrueIsTrue() {
        $foo = true;
        $this->assertTrue($foo);
    }

    public function testFalseIsFalse() {
        $foo = false;
        $this->assertFalse($foo);
    }
}